# HXMeituan App<br>
![](picHXMeituan/AppIcon.png)<br>
React-Native实现跨平台开发  <br>   
###一、基于React-Native & Node电商App<br>
###主要完成的功能有<br>
+ 界面搭建技术点:<br>
1.FlexBox布局<br>
2.ListView<br>
3.ScrollView<br>
+ 第三方类库的使用,轻松实现源生项目的TabbarController和NavigationController效果<br>
+ webview内嵌实例<br>
+ 界面交互<br>
+ 跨界面传值<br>
效果如下图所示：<br>
首页<br>
![](picHXMeituan/首页.png)<br> 
首页猜你喜欢<br>
![](picHXMeituan/猜你喜欢.png)<br>
点击购物中心跳转到购物中心详情界面<br>
![](picHXMeituan/购物中心详情.png)<br>
商家<br>
![](picHXMeituan/商家.png)<br>
我的<br>
![](picHXMeituan/我的.png)<br>
更多<br>
![](picHXMeituan/更多.png)<br>


安装 & 启动<br>
    (1)首先进入HXMeituan目录安装node module；命令行：$ npm install <br>
    (2)安装第三方类库,命令行: $ npm i react-native-tab-navigator —save<br>
####Tip:<br>
    (1)为了演示，代码有些粗糙<br>
    (2)抓取的"猜你喜欢"接口有部分问题,数据可能显示错误<br>
    (3)部分点击功能实现原理相同,仅用一个alert弹框表示<br>
    (4)欢迎交流学习:zhxWorkMail@163.com<br>
    