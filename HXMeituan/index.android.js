/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Navigator
} from 'react-native';

// 导入外部的文件
var LaunchPage = require('./Component/Main/LaunchPage');

class HXMeituan extends Component {
    render() {
        return (
            <Navigator
                initialRoute={{ name: 'Launch', component: LaunchPage }}
                configureScene={(route) => { //过渡动画
                            return Navigator.SceneConfigs.PushFromRight;
                        }}
                renderScene={(route, navigator) => {
                            let Component = route.component;
                            return <Component {...route.params} navigator={navigator} />
                        }}
            />
        );
    }
}


AppRegistry.registerComponent('HXMeituan', () => HXMeituan);
