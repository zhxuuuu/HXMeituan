/**
 * Created by Zhxuuuu on 16/5/31.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';


// 导入外部的文件
var Main = require('./Main');

// 创建组件类
var LaunchPage = React.createClass({
    getDefaultProps(){
        return{
            delayTime: 1500
        }
    },

    render(){
        return(
            <Image source={{uri: 'launchimage'}} style={styles.outViewStyle}/>
        );
    },

    // 复杂的操作:请求网络数据 和 定时器
    componentDidMount(){
        setTimeout(()=>{
            this.props.navigator.replace({
                component:Main
            });
        }, this.props.delayTime);
    }
});


// 创建样式类
var styles = StyleSheet.create({
    outViewStyle:{
        flex:1
    }
});


// 模块输出
module.exports = LaunchPage;