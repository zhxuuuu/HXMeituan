/**
 * Created by Zhxuuuu on 16/5/31.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Platform,  // 判断系统
    Navigator  //导入导航器
} from 'react-native';

// 导入Tab
import TabNavigator from 'react-native-tab-navigator';

// 导入外部的文件
var Home = require('../Home/Home');
var Shop = require('../Shop/Shop');
var Mine = require('../Mine/Mine');
var More = require('../More/More');


// 创建组件类
var Main = React.createClass({
    getInitialState(){
        return{
            selectedTab: 'home'
        }
    },

    render(){
        return(
            <TabNavigator>
                {/*首页*/}
                {this.renderTabItem('icon_tabbar_homepage', 'icon_tabbar_homepage_selected', '首页', 'home', 'home', Home, '')}
                {/*商家*/}
                {this.renderTabItem('icon_tabbar_merchant_normal', 'icon_tabbar_merchant_selected', '商家', 'shop', 'shop', Shop, '')}
                {/*我的*/}
                {this.renderTabItem('icon_tabbar_mine', 'icon_tabbar_mine_selected', '我的', 'mine', 'mine', Mine, '10')}
                {/*更多*/}
                {this.renderTabItem('icon_tabbar_misc', 'icon_tabbar_misc_selected', '更多', 'more', 'more', More, '')}


            </TabNavigator>
        );
    },

    // 单独的Item
    renderTabItem(icon, selectedIcon, title, selectedTab, initialRouteName, component, badgeText){
        return(
            <TabNavigator.Item
                renderIcon={() => <Image source={{uri: icon}} style={styles.tabIconStyle}/> }
                renderSelectedIcon={() => <Image source={{uri: selectedIcon}} style={styles.tabIconStyle} />}
                title={title}
                selectedTitleStyle={{color:'rgba(255,96,0,1.0)'}}
                renderBadge={()=> this.renderBadgeView(badgeText)}
                selected={this.state.selectedTab === selectedTab}
                onPress={() => this.setState({ selectedTab: selectedTab })}
            >
                <Navigator
                    initialRoute={{ name: initialRouteName, component: component }}
                    configureScene={(route) => { //过渡动画
            return Navigator.SceneConfigs.PushFromRight;
        }}
                    renderScene={(route, navigator) => {
            let Component = route.component;
            return <Component {...route.params} navigator={navigator} />
        }}
                />
            </TabNavigator.Item>
        )
    },

    renderBadgeView(badgeText){
        if (badgeText.length == 0) return;
        return(
            <View style={styles.badgeViewStyle}>
                <Text style={styles.badgeTextStyle}>{badgeText}</Text>
            </View>
        )
    }
});


// 创建样式类
var styles = StyleSheet.create({
    tabIconStyle:{
        width:Platform.OS == 'ios'? 30 : 25,
        height:Platform.OS == 'ios'? 30 : 25
    },

    badgeViewStyle:{
        width:16,
        height:16,
        backgroundColor:'red',
        borderRadius:8,
        marginTop:5,
        justifyContent:'center',
        alignItems:'center'
    },

    badgeTextStyle:{
        backgroundColor:'rgba(0,0,0,0)',
        color:'white'
    }
});


// 模块输出
module.exports = Main;