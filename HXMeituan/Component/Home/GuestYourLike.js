/**
 * Created by Zhxuuuu on 16/6/2.
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Platform,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native';

// 导入外部的文件
var GuestYoulikeListView = require('./GuestYouLikeListView');

var CommonCell = require('./BottomCommonCell');

var GuestYouLike = React.createClass({
    render(){
        return(
          <View>
            <CommonCell
                leftIcon="cnxh"
                leftTitle="猜你喜欢"
            />
             <GuestYoulikeListView />
          </View>
        )
    }
});


var styles = StyleSheet.create({

});

module.exports = GuestYouLike;