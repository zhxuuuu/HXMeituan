/**
 * Created by Zhxuuuu on 16/5/31.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Platform,
    Image,
    TouchableOpacity
} from 'react-native';


var Dimensions = require('Dimensions');
var {width} = Dimensions.get('window');

// 导入外部的文件
var BannerCommonCell = require('./BannerCommonCell');


var BannerView = React.createClass({
    getDefaultProps(){
        return{
            preData: {}  // 上个界面传值
        }
    },
    
    render(){
       return(
           <View style={styles.viewStyle}>
               {/*左边的View*/}
               {this.renderLeftView()}

               {/*右边的View*/}
               <View style={styles.rightViewStyle}>
                   {this.renderRightView()}
               </View>
           </View>
       )
    },

    // 左边
    renderLeftView(){
       // 取出需要的数据
       var itemData = this.props.preData.dataLeft[0];
       return(
          <TouchableOpacity onPress={()=>alert(0)}>
           <View style={styles.leftViewStyle}>
               <Image source={{uri: itemData.img1}} style={styles.leftImgStyle}/>
               <Image source={{uri: itemData.img2}} style={styles.leftImgStyle} />
               <Text style={{color:'gray', fontSize:16}}>{itemData.title}</Text>
               <View style={styles.priceViewStyle}>
                   <Text style={{color:'green'}}>{itemData.price}</Text>
                   <Text style={styles.saleStyle}>{itemData.sale}</Text>
               </View>
           </View>
          </TouchableOpacity>
       )
    },


    // 右边
    renderRightView(){
        // 数组
        var itemArr = [];
        // 取出需要的数组数据
        var dataArr = this.props.preData.dataRight;
        // 遍历创建子组件
        for(var i=0; i<dataArr.length; i++){
            // 取出单个的数据
            var item = dataArr[i];
            // 创建组件装入数组
            itemArr.push(
               <BannerCommonCell key={i} data={item}/>  
            );
        }
        // 返回
        return itemArr;
    }
});


var styles = StyleSheet.create({
    viewStyle:{
        // backgroundColor:'white',
        marginTop:15,
        // 确定主轴的方向
        flexDirection:'row'
    },

    leftViewStyle:{
        backgroundColor:'white',
        width:width * 0.5 -1,
        height:119,
        marginRight:1,
        // 侧轴居中
        alignItems:'center',
        justifyContent:'center'
    },

    leftImgStyle:{
        width:78,
        height:25,
        // 图片的内容模式
        resizeMode:'contain'
    },

    priceViewStyle:{
        marginTop:5,
        marginBottom:5,
        flexDirection:'row'
    },

    saleStyle:{
        color:'orange',
        backgroundColor:'yellow',
        fontSize:12,
        padding:2,
        marginLeft:3
    }
});


module.exports = BannerView;
