/**
 * Created by Zhxuuuu on 16/6/2.
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Platform,
    Image,
    TouchableOpacity,
    ListView
} from 'react-native';


var Dimensions = require('Dimensions');
var {width} = Dimensions.get('window');


// 导入json数据
var YouLikeData = require('../../LocalData/HomeGeustYouLike.json');


var YouLikeListView = React.createClass({
    getDefaultProps(){
       return{
           url:'http://api.meituan.com/group/v2/recommend/homepage/city1/20?userId=160495643&userid=160495643&__vhost=api.mobile.meituan.com&position=23.134643%2C113.373951&movieBundleVersion=100&utm_term=6.6&limit=40&wifi-mac=64%3A09%3A80%3A10%3A15%3A27&ci=20&__skcy=X6Jxu5SCaijU80yX5ioQuvCDKj4%3D&__skua=5657981d60b5e2d83e9c64b453063ada&__skts=1459731016.350255&wifi-name=Xiaomi_1526&client=iphone&uuid=5C7B6342814C7B496D836A69C872202B5DE8DB689A2D777DFC717E10FC0B4271&__skno=FEB757F5-6120-49EC-85B0-D1444A2C2E7B&utm_content=5C7B6342814C7B496D836A69C872202B5DE8DB689A2D777DFC717E10FC0B4271&utm_source=AppStore&utm_medium=iphone&version_name=6.6&wifi-cur=0&wifi-strength=&offset=0&utm_campaign=AgroupBgroupD100H0&__skck=3c0cf64e4b039997339ed8fec4cddf05&msid=0FA91DDF-BF5B-4DA2-B05D-FA2032F30C6C2016-04-04-08-38594'
       }
    },

    getInitialState(){
        return{
            dataSource: new ListView.DataSource({
                rowHasChanged:(row1, row2) => row1 !== row2
            })
        }
    },

    render(){
        return(
            <ListView
                dataSource={this.state.dataSource}
                renderRow={this.renderRow}
            />
        )
    },

    // 返回具体的cell
    renderRow(rowData){
        return(
           <TouchableOpacity>
               <View style={styles.cellViewStyle}>
                  <Image source={{uri:this.dealWithImgUrl(rowData.imageUrl)}}  style={styles.imageStyle}/>
                  <View style={styles.rightViewStyle}>
                      <View style={styles.rightTopViewStyle}>
                          <Text style={styles.titleStyle}>{rowData.title}</Text>
                          <Text style={{height:25}}>{rowData.topRightInfo}</Text>
                      </View>
                      <Text style={{color:'green', marginBottom:10}}>{rowData.subTitle}</Text>
                      <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                      <Text>{rowData.subMessage}</Text>
                      <Text>{rowData.bottomRightInfo}</Text>
                      </View>
                  </View>
               </View>
           </TouchableOpacity>
        )
    },

    // 处理图片
    dealWithImgUrl(url){
        // 任意正数  -1
        if(url.search('w.h') !== -1){ // 找到
            return url.replace('w.h', '120.90');
        }else{
            return url;
        }
    },

    componentDidMount(){
        fetch(this.props.url)
            .then((response) => response.json())
            .then((responseData) =>{
                // 更新状态机
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(responseData.data)
                });
            })
            .catch((error)=>{
                // 更新状态机
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(YouLikeData.data)
                });
            })
    }



});


var styles = StyleSheet.create({
    imageStyle:{
        width:120,
        height:90,
        margin:10,
        borderRadius:5
    },

    cellViewStyle:{
        // 主轴方向
        flexDirection:'row',
        backgroundColor:'white',
        // 底部的边框
        borderBottomWidth:1,
        borderBottomColor:'#e8e8e8'
    },

    rightViewStyle:{
        width:width * 0.6,
        // backgroundColor:'red'
        justifyContent:'center',

    },

    rightTopViewStyle:{
        flexDirection:'row',
        alignItems:'center',
        height:25,
        overflow:'hidden'
    },

    titleStyle:{
        width: width * 0.48,
        overflow:'hidden',
        height:25,
        color:'orange'
    }
});


module.exports = YouLikeListView