/**
 * Created by Zhxuuuu on 16/6/2.
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Platform,
    Image,
    TouchableOpacity,
} from 'react-native';

// 引入外部的文件
var BannerCommonCell = require('./BannerCommonCell');


var MiddleView = React.createClass({
    getDefaultProps(){
       return{
           topData:[],
           bottomData: [] //下部分数据
       }
    },

    render(){
       return(
           <View style={styles.viewStyle}>
               {/*上面的View*/}
               {this.renderTopItemView()}
               {/*下面的View*/}
               <View style={styles.bottomViewStyle}>
                   {this.renderBottomItemView()}
               </View>
           </View>
       )
    },

    // 上部分内容
    renderTopItemView(){
        return(
           <View style={styles.topViewStyle}>
               <View style={styles.topLeftViewStyle}>
                   <Text style={{fontSize:22, color:'pink'}}>{this.props.topData.title}</Text>
                   <Text style={{color:'grey', marginTop:3}}>{this.props.topData.subTitle}</Text>
               </View>
               <Image source={{uri: this.props.topData.image}} style={styles.topImgStyle}/>
           </View>
        );
    },

    // 下部分内容
    renderBottomItemView(){
        // 组件数组
        var itemArr = [];
        // 数据
        var dataArr = this.props.bottomData;
        // 遍历
        for(var i=0; i<dataArr.length; i++){
            // 取出单独的数据
            var item = dataArr[i];
            // 转化模型
            var changeItem = {"title": item.maintitle, "subTitle":item.deputytitle,"rightImage":this.dealWithImgUrl(item.imageurl), "titleColor":item.typeface_color}
            // 创建组件装入数组
            itemArr.push(
                <BannerCommonCell key={i}
                    data = {changeItem}
                />
            );
        }
        // 返回数组
        return itemArr;
    },

    // 处理图片
    dealWithImgUrl(url){
        // 任意正数  -1
        if(url.search('w.h') !== -1){ // 找到
            return url.replace('w.h', '120.90');
        }else{
            return url;
        }
    }
});


var styles = StyleSheet.create({
    viewStyle:{
       marginTop:15,
    },

    bottomViewStyle:{
        // 主轴的方向
        flexDirection:'row',
        // 换行
        flexWrap:'wrap'
    },

    topViewStyle:{
        // 主轴的方向
        flexDirection:'row',
        // 背景颜色
        backgroundColor:'white',
        // 下边框
        borderBottomColor:'#e8e8e8',
        borderBottomWidth:1,
        // 垂直居中
        alignItems:'center',
        justifyContent:'space-between'
    },

    topImgStyle:{
        width:150,
        height:63
    },

    topLeftViewStyle:{
        marginLeft:8
    }
});


module.exports = MiddleView;
