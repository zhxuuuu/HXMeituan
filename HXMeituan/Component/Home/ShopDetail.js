/**
 * Created by Zhxuuuu on 16/6/2.
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Platform,
    Image,
    TouchableOpacity,
    ScrollView,
    WebView
} from 'react-native';


var ShopDetail = React.createClass({
   getDefaultProps(){
     return{
         footerUrl: '?uuid=5C7B6342814C7B496D836A69C872202B5DE8DB689A2D777DFC717E10FC0B4271&utm_term=6.6&utm_source=AppStore&utm_content=5C7B6342814C7B496D836A69C872202B5DE8DB689A2D777DFC717E10FC0B4271&version_name=6.6&userid=160495643&utm_medium=iphone&lat=23.134709&utm_campaign=AgroupBgroupD100Ghomepage_shoppingmall_detailH0&token=b81UqRVf6pTL4UPLLBU7onkvyQoAAAAAAQIAACQVmmlv_Qf_xR-hBJVMtIlq7nYgStcvRiK_CHFmZ5Gf70DR47KP2VSP1Fu5Fc1ndA&lng=113.373890&f=iphone&ci=20&msid=0FA91DDF-BF5B-4DA2-B05D-FA2032F30C6C2016-04-04-08-38594'
     }
   },

    render(){
        console.log(this.props.detailUrl +this.props.footerUrl);
        return(
            <View style={styles.viewStyle}>
                {/*导航*/}
                {this.renderNavBar()}
                <WebView
                    automaticallyAdjustContentInsets={true}
                    source={{uri: this.props.dealUrl +this.props.footerUrl}}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    startInLoadingState={true}
                />
            </View>
        )
    },

    renderNavBar(){
        return(
            <View style={styles.navBarStyle}>
                {/*左边*/}
                <TouchableOpacity onPress={()=>{this.props.navigator.pop()}}>
                    <Image source={{uri: 'icon_camera_back_normal'}} style={[styles.rightNavImgStyle, {marginLeft:8}]}/>
                </TouchableOpacity>
                {/*中间*/}
                <Text style={{color:'white', fontSize:18, fontWeight:'bold'}}>购物中心详情</Text>
                {/*右边*/}
                <View style={styles.rightViewStyle}>
                    <TouchableOpacity onPress={()=>{alert('点右边1')}}>
                        <Image source={{uri: null}} style={[styles.rightNavImgStyle, {marginRight:8}]}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
});

var styles = StyleSheet.create({
    viewStyle:{
        backgroundColor:'red',
        flex:1
    },

    navBarStyle:{
        height:Platform.OS == 'ios'? 64 : 44,
        backgroundColor:'rgba(255,96,0,1.0)',
        // 设置主轴的方向
        flexDirection:'row',
        // 对齐方式
        alignItems:'center',
        // 主轴的对齐方式
        justifyContent:'space-between',

        // iOS
        paddingTop:Platform.OS == 'ios'? 10 : 0
    },

    rightNavImgStyle:{
        width:24,
        height:24
    },
});

module.exports = ShopDetail;