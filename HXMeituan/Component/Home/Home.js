/**
 * Created by Zhxuuuu on 16/5/31.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    Platform,
    ScrollView
} from 'react-native';

// 导入外部的json数据
var TopMenu = require('../../LocalData/TopMenu.json');
var TopMiddleData = require('../../LocalData/HomeTopMiddleLeft.json');
var MiddleData = require('../../LocalData/Home_D4.json');
var MiddleData2 = require('../../LocalData/HomeTopMiddle.json');
var ShopCenterData = require('../../LocalData/Home_D5.json');

// 获取屏幕的宽度
var Dimensions = require('Dimensions');
var screenWidth = Dimensions.get('window').width;

// 导入外部的文件
var HomeDetail = require('./HomeDetail');
var HomeTopView = require('./HomeTopView');
var BannerView = require('./BannerView');
var MiddleView = require('./MiddleView');
var ShopCenter = require('./ShopCenter');
var ShopDetail = require('./ShopDetail');
var GuestYourLike = require('./GuestYourLike');


// 创建组件类
var Home = React.createClass({
    render(){
        return(
            <View style={styles.outViewStyle}>
                {/*导航条*/}
                {this.renderNavBar()}
                <ScrollView>
                    {/*头部*/}
                    <HomeTopView
                       data = {TopMenu.data}
                    />
                    {/*头部之下的板块*/}
                    <BannerView 
                       preData = {TopMiddleData} 
                    />
                    {/*中间的板块*/}
                    <MiddleView
                        topData={MiddleData2.data[0]}
                        bottomData={MiddleData.data}
                    />
                    {/*购物中心*/}
                    <ShopCenter
                        shopsData={ShopCenterData.data}
                        dataToHome={(detailUrl)=>{this.pushTopShopDetail(detailUrl)}}
                    />
                    {/*猜你喜欢*/}
                    <GuestYourLike />
                </ScrollView>
            </View>
        );
    },

    // 跳转到购物中心详情页
    pushTopShopDetail(detailUrl){
        // 处理掉的URL
        var dealUrl = this.dealWithdetailUrl(detailUrl);
        // alert(dealUrl);
       this.props.navigator.push({
           component:ShopDetail,
           params:{dealUrl}
       });
    },
    
    // 处理URL
    dealWithdetailUrl(detailUrl){
       if(detailUrl.search('imeituan') !== -1){// 找到
           // debugger;
           return detailUrl.replace('imeituan://www.meituan.com/web/?url=', '');
       }else{
           // debugger;
           return detailUrl;
       } 
    },

    // 导航条
    renderNavBar(){
        return(
            <View style={styles.navBarStyle}>
                {/*左边*/}
               <TouchableOpacity onPress={()=>{alert('点左边')}}>
                 <Text style={styles.leftTextStyle}>广州</Text>
               </TouchableOpacity>
                {/*中间*/}
                <TextInput
                  style={styles.textInputStyle}
                  placeholder="输入商家 商圈 品类"
                />
                {/*右边*/}
                <View style={styles.rightViewStyle}>
                    <TouchableOpacity onPress={()=>{alert('点右边1')}}>
                       <Image source={{uri: 'icon_homepage_message'}} style={styles.rightNavImgStyle}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{alert('点右边2')}}>
                       <Image source={{uri: 'icon_homepage_scan'}} style={styles.rightNavImgStyle}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    },

    // 跳转到下一个界面
    pushToNextPage(){
       this.props.navigator.push({
           component:HomeDetail
       });
    }
});


// 创建样式类
var styles = StyleSheet.create({
    outViewStyle:{
        flex:1,
        backgroundColor:'#dddddd'
    },

    rightNavImgStyle:{
        width:24,
        height:24
    },

    navBarStyle:{
       height:Platform.OS == 'ios'? 64 : 44,
       backgroundColor:'rgba(255,96,0,1.0)',
       // 设置主轴的方向
       flexDirection:'row',
       // 对齐方式
       alignItems:'center',
       // 主轴的对齐方式
       justifyContent:'space-around'
    },

    textInputStyle:{
        height:Platform.OS == 'ios'? 34:30,
        width:screenWidth * 0.75,
        backgroundColor:'white',
        // iOS
        marginTop:Platform.OS == 'ios'? 22 : 0,
        // 圆角
        borderRadius:17,
        // 左内边距
        paddingLeft:8
    },

    leftTextStyle:{
        // iOS
        marginTop:Platform.OS == 'ios'? 10 : 0,
        color:'white'
    },

    rightViewStyle:{
        // 设置主轴的方向
        flexDirection:'row',
        // iOS
        marginTop:Platform.OS == 'ios'? 10 : 0
    }
});


// 模块输出
module.exports = Home;