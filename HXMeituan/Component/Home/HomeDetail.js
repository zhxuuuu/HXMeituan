/**
 * Created by Zhxuuuu on 16/5/31.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';


// 创建组件类
var HomeDetail = React.createClass({
    render(){
        return(
            <View style={styles.outViewStyle}>
              <TouchableOpacity onPress={()=>{this.popToPrePage()}}>
                <Text>首页详情页</Text>
              </TouchableOpacity>
            </View>
        );
    },

    popToPrePage(){
        this.props.navigator.pop();
    }
});


// 创建样式类
var styles = StyleSheet.create({
    outViewStyle:{
        flex:1,
        backgroundColor:'green',
        justifyContent:'center',
        alignItems:'center'
    }
});


// 模块输出
module.exports = HomeDetail;