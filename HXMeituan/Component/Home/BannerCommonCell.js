/**
 * Created by Zhxuuuu on 16/5/31.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Platform,
    Image,
    TouchableOpacity
} from 'react-native';


var Dimensions = require('Dimensions');
var {width} = Dimensions.get('window');

var BCommonCell = React.createClass({
    getDefaultProps(){
        return{
            data: {}
        }
    },

    render(){
        // console.log(this.props.data.rightImage);
        return(
          <TouchableOpacity onPress={()=>alert('点击了右边')}>
            <View style={styles.viewStyle}>
                {/*左边*/}
                <View style={styles.leftViewStyle}>
                    <Text style={[styles.titleStyle, {color:this.props.data.titleColor}]}>{this.props.data.title}</Text>
                    <Text style={styles.subTitleStyle}>{this.props.data.subTitle}</Text>
                </View>
                {/*右边*/}
                <Image source={{uri: this.props.data.rightImage}}  style={styles.rightImgStyle}/>
            </View>
          </TouchableOpacity>
        );
    }
});



var styles = StyleSheet.create({
    viewStyle:{
        // 确定主轴的方向
        flexDirection:'row',
        // 高度
        height:60,
        // 宽度
        width:width*0.5-1,
        // 背景颜色
        backgroundColor:'white',
        // 边框
        borderBottomColor:'#e8e8e8',
        borderBottomWidth:1,
        // 居中
        justifyContent:'space-around',
        alignItems:'center',
        marginRight:1
    },

    rightImgStyle:{
        width:80,
        height:50,
        resizeMode:'contain',
        // backgroundColor:'red'
    },

    titleStyle:{
      fontSize: Platform.OS=='ios'?18:15
    },

    subTitleStyle:{
        color:'gray',
        marginTop:Platform.OS=='ios'?5:0,
        fontSize: Platform.OS=='ios'?15:12
    }
});


module.exports = BCommonCell;