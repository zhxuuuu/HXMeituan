/**
 * Created by Zhxuuuu on 16/6/2.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Platform,
    Image,
    TouchableOpacity,
    ScrollView,
} from 'react-native';

// 导入外部的文件
var TopListView = require('./HomeTopListView');


var Dimensions = require('Dimensions');
var {width} = Dimensions.get('window');


var HomeTopView = React.createClass({
    getDefaultProps(){
        return{
            data: []
        }
    },
    
    getInitialState(){
        return{
           currentPage: 0
        }
    },

    render(){
        return(
            <View style={styles.viewStyle}>
                {/*上部分*/}
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    pagingEnabled={true}
                    onMomentumScrollEnd ={this.onScrollAnimationEnd}
                >
                    {this.renderScrollItem()}
                </ScrollView>
                {/*下部分*/}
                <View style={styles.pageViewStyle}>
                    {this.renderPageIndicator()}
                </View>
            </View>
        )
    },

    // 当一帧滚动结束
    onScrollAnimationEnd(e){
        // 求出水平方向的偏移量 --->页数
        var currentPage = e.nativeEvent.contentOffset.x / width;
        // 更新状态机
        this.setState({
            currentPage: currentPage
        });
    },

    renderScrollItem(){
        // 数组
        var itemArr = [];
        // 颜色数组
        var colorArr = ['red', 'blue'];
        // 遍历
        for(var i=0; i<2; i++){
            // 创建组件装入数组
            itemArr.push(
               <TopListView key={i} data={this.props.data[i]}/>
            );
        }
        // 返回
        return itemArr;
    },

    // 分页圆点
    renderPageIndicator(){
        // 数组
        var pageArr = [], style;
        // 遍历
        for(var i=0; i<2; i++){
            // 设置样式
            style = (this.state.currentPage === i) ? {color:'orange'} : {color:'grey'}

            pageArr.push(
                <Text key={i} style={[{fontSize:25}, style]}>&bull;</Text>
            );
        }
        // 返回
        return pageArr;
    }
});



var styles = StyleSheet.create({
    viewStyle:{
        backgroundColor:'white'
    },

    pageViewStyle:{
       //主轴的方向
       flexDirection:'row',
       justifyContent:'center'
    }
});



module.exports = HomeTopView;