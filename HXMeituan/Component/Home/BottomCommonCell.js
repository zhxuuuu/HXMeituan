/**
 * Created by Zhxuuuu on 16/6/2.
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Platform,
    Image,
    TouchableOpacity,
} from 'react-native';


var CommonCell = React.createClass({
    getDefaultProps(){
       return{
          leftIcon: '',
          leftTitle: '',
          rightTitle:''
       }
    },

    render(){
        return(
          <TouchableOpacity>
            <View style={styles.viewStyle}>
                {/*左边*/}
                <View style={styles.leftViewStyle}>
                    <Image source={{uri: this.props.leftIcon}} style={styles.leftImgStyle} />
                    <Text>{this.props.leftTitle}</Text>
                </View>
                {/*右边*/}
                <View style={styles.rightViewStyle}>
                    <Text style={{color:'grey'}}>{this.props.rightTitle}</Text>
                    <Image source={require('image!icon_cell_rightArrow')} style={styles.rightImgStyle}/>
                </View>
            </View>
          </TouchableOpacity>
        )
    }
});

var styles = StyleSheet.create({
    viewStyle:{
      // 主轴方向
        flexDirection:'row',
      // 背景
        backgroundColor:'white',
      // 高度
        height:44,
      // 侧轴的居中
        alignItems:'center',
      // 主轴对齐方式
        justifyContent:'space-between',
      // 下边框
        borderBottomColor:'#e8e8e8',
        borderBottomWidth:0.5
    },

    leftViewStyle:{
        // 主轴方向
        flexDirection:'row',
        // 侧轴的居中
        alignItems:'center'
    },

    leftImgStyle:{
        width:30,
        height:30
    },

    rightViewStyle:{
        // 主轴方向
        flexDirection:'row',
        // 侧轴的居中
        alignItems:'center'
    },

    rightImgStyle:{
        marginRight:8,
        marginLeft:5
    }
});


module.exports = CommonCell;