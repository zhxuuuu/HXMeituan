/**
 * Created by Zhxuuuu on 16/6/2.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ListView,
    Platform
} from 'react-native';

var Dimensions = require('Dimensions');
var {width} = Dimensions.get('window');

// 全局的变量
var cols = 5;
var itemWH = Platform.OS == 'ios' ? 70:60;
var vMargin = (width - itemWH * cols) / (cols +1);
var hMargin = 10;

var TopListView = React.createClass({
    getDefaultProps(){
       return{
           data:[]
       }
    },

    getInitialState(){
        // 数据源
        var ds = new ListView.DataSource({rowHasChanged:(row1, row2) => row1 !== row2});
        return{
           dataSource:ds.cloneWithRows(this.props.data)
        }
    },

    render(){
        return(
           <ListView
               dataSource={this.state.dataSource}
               renderRow={this.renderRow}
               contentContainerStyle={styles.contentViewStyle}
               scrollEnabled={false}
           />
        )
    },

    // 返回一个具体的cell
    renderRow(rowData){
        return (
          <TouchableOpacity onPress={()=>alert('点了')}>
            <View style={styles.cellStyle}>
               <Image source={{uri: rowData.image}} style={{width:Platform.OS == 'ios' ? 52:42, height:Platform.OS == 'ios' ? 52:42}}/>
               <Text style={styles.cellTitleStyle}>{rowData.title}</Text>
            </View>
          </TouchableOpacity>
        )
    }
});


var styles = StyleSheet.create({
    contentViewStyle:{
        // 设置主轴的方向
        flexDirection:'row',
        // 换行
        flexWrap:'wrap',
        // 宽度
        width:width
    },

    cellStyle:{
       // 居中
       justifyContent:'center',
       alignItems:'center',
       width:itemWH,
       height:itemWH,
       // 设置左和上边距
       marginLeft:vMargin,
       marginTop:hMargin
    },

    cellTitleStyle:{
        fontSize:Platform.OS=='ios'?13:12,
        color:'gray'
    }
});


module.exports = TopListView;
