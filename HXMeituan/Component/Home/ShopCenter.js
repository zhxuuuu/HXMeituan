/**
 * Created by Zhxuuuu on 16/6/2.
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Platform,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native';

// 引入外部的文件
var CommonCell = require('./BottomCommonCell');
// var ShopDetail = require('./ShopDetail');


var ShopCenter = React.createClass({
    getDefaultProps(){
        return{
           shopsData: [], // 数组数据
           dataToHome: {} // 逆向传递 
        }
    },

    render(){
        return(
            <View style={styles.viewStyle}>
                {/*上部分*/}
                <CommonCell
                    leftIcon="gwzx"
                    leftTitle="购物中心"
                    rightTitle={"全部"+this.props.shopsData.length+"家"}
                />
                {/*下部分*/}
                <ScrollView
                    horizontal={true}
                    style={{backgroundColor:'white', paddingBottom:10}}
                    showsHorizontalScrollIndicator={false}
                >
                    {this.renderScrollItem()}
                </ScrollView>
            </View>
        )
    },

    // 返回ScrollView中的子组件
    renderScrollItem(){
       // 数组
       var itemArr = [];
       // 临时的数组
       var shopsData = this.props.shopsData;
       // 遍历
       for(var i=0; i<shopsData.length; i++){
           // 取出一条数据
           var item = shopsData[i];
           // 创建子组件装入数组
           itemArr.push(
               <ScrollItemView
                   key={i}
                   itemData={item}
                   dataToShopCenter={(detailUrl)=>{this.dealWithNextData(detailUrl)}}
               />
           );
       }
       // 返回数组
       return itemArr;
    },

    dealWithNextData(detailUrl){
        this.props.dataToHome(detailUrl);
    }
});


var ScrollItemView = React.createClass({
    getDefaultProps(){
        return{
           itemData: {},
           dataToShopCenter: {} // 逆传递函数
        }
    },


    render(){
        return(
          <TouchableOpacity onPress={()=>{this.pushToShopDetail(this.props.itemData.detailurl)}}>
           <View style={styles.itemViewStyle}>
              <Image source={{uri: this.props.itemData.img}} style={styles.scrollImgStyle} />
              <View style={styles.showTextStyle}><Text style={{color:'white', fontSize:12}}>{this.props.itemData.showtext.text}</Text></View>
              <Text>{this.props.itemData.name}</Text>
           </View>
          </TouchableOpacity>
        )
    },

    pushToShopDetail(detailUrl){
        this.props.dataToShopCenter(detailUrl);
    }
});


var styles = StyleSheet.create({
    viewStyle:{
        // 上下边距
        marginTop:15,
        marginBottom:15
    },

    scrollImgStyle:{
       // 外间距
        margin:10,
        width:103,
        height:75
    },

    itemViewStyle:{
        // 侧轴居中
        alignItems:'center'
    },

    showTextStyle:{
        backgroundColor:'orange',
        // 绝对定位
        position:'absolute',
        left:10,
        top:55,
        padding:3,
        borderTopRightRadius:10,
        borderBottomRightRadius:10
    }
});


module.exports = ShopCenter;