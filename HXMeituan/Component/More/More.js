/**
 * Created by Zhxuuuu on 16/5/31.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Platform,
    ScrollView
} from 'react-native';

// 导入外部的文件
var CommonCell = require('./CommonCell');


// 创建组件类
var More = React.createClass({
    render(){
        return(
            <View style={styles.outViewStyle}>
                {/*导航条*/}
                {this.renderNavBar()}
                {/*内容*/}
                <ScrollView>
                    {/*下边的cell*/}
                    <View style={{marginTop:15}}>
                        <CommonCell
                            mainTitle="扫一扫"
                        />
                    </View>

                    <View style={{marginTop:15}}>
                        <CommonCell
                            mainTitle="省流量模式"
                            isSwitch={true}
                        />
                        <CommonCell
                            mainTitle="消息提醒"
                        />
                        <CommonCell
                            mainTitle="邀请好友使用优惠券"
                        />
                        <CommonCell
                            mainTitle="清空缓存"
                            subTitle="1.99MB"
                        />
                    </View>

                    <View style={{marginTop:15}}>
                        <CommonCell
                            mainTitle="意见反馈"
                        />
                        <CommonCell
                            mainTitle="问卷调查"
                        />
                        <CommonCell
                            mainTitle="清空缓存"
                            subTitle="1.99MB"
                        />
                        <CommonCell
                            mainTitle="省流量模式"
                        />
                        <CommonCell
                            mainTitle="省流量模式"
                        />
                        <CommonCell
                            mainTitle="清空缓存"
                            subTitle="1.99MB"
                        />
                    </View>

                    <View style={{marginTop:15}}>
                        <CommonCell
                            mainTitle="省流量模式"
                        />
                        <CommonCell
                            mainTitle="省流量模式"
                        />

                    </View>
                </ScrollView>
            </View>
        );
    },

    renderNavBar(){
        return(
            <View style={styles.navBarStyle}>
                {/*左边*/}
                <TouchableOpacity onPress={()=>{alert('点左边')}}>
                    <Image source={{uri: null}} style={[styles.rightNavImgStyle, {marginLeft:8}]}/>
                </TouchableOpacity>
                {/*中间*/}
                <Text style={{color:'white', fontSize:18, fontWeight:'bold'}}>更多</Text>
                {/*右边*/}
                <View style={styles.rightViewStyle}>
                    <TouchableOpacity onPress={()=>{alert('点右边1')}}>
                        <Image source={{uri: 'icon_mine_setting'}} style={[styles.rightNavImgStyle, {marginRight:8}]}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
});


// 创建样式类
var styles = StyleSheet.create({
    outViewStyle:{
        flex:1,
        backgroundColor:'#e8e8e8'
    },

    navBarStyle:{
        height:Platform.OS == 'ios'? 64 : 44,
        backgroundColor:'rgba(255,96,0,1.0)',
        // 设置主轴的方向
        flexDirection:'row',
        // 对齐方式
        alignItems:'center',
        // 主轴的对齐方式
        justifyContent:'space-between',

        // iOS
        paddingTop:Platform.OS == 'ios'? 10 : 0
    },

    rightNavImgStyle:{
        width:24,
        height:24
    },
});


// 模块输出
module.exports = More;