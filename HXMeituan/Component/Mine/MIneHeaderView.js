/**
 * Created by Zhxuuuu on 16/5/31.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Platform,
    TouchableOpacity,
    Image
} from 'react-native';

var Dimensions = require('Dimensions');
//获取屏幕的宽度
var {width} = Dimensions.get('window');

var MineHeadView = React.createClass({
    render(){
        return(
            <View style={styles.viewStyle}>
                {/*上部分*/}
                <TopHeadView />
                {/*下部分*/}
                <TopBottomView />
            </View>
        );
    }
});


/**--------创建头部的组件类-----------**/
var TopHeadView = React.createClass({
    getDefaultProps(){
        return{
            topData: {"icon":"see", "title":"MT电商", "rank":"avatar_vgirl"}
        }
    },

    render(){
        return(
            <TouchableOpacity onPress={()=>alert('点了')}>
                <View style={styles.topViewStyle}>
                    {/*左边*/}
                    <Image source={{uri: this.props.topData.icon}} style={styles.topImgStyle}/>
                    {/*中间*/}
                    <View style={styles.middleViewStyle}>
                        <Text>{this.props.topData.title}</Text>
                        <Image source={{uri: this.props.topData.rank}} style={styles.rankStyle}/>
                    </View>
                    {/*右边*/}
                    <Image source={require('image!icon_cell_rightArrow')} style={styles.rightImgStyle}/>
                </View>
            </TouchableOpacity>
        )
    }
});

/**--------创建尾部的组件类-----------**/
var CenterData = require('./CenterData.json');

var TopBottomView = React.createClass({
    render(){
        return(
            <View style={styles.mineBottomViewStyle}>
                {this.renderAllItem()}
            </View>
        );
    },

    // 返回所有的子组件
    renderAllItem(){
        // 数组
        var itemArr = [];
        // 遍历
        for(var i=0; i<CenterData.length; i++){
            // 取出单独的item
            var item = CenterData[i];
            // 创建组件装入数组
            itemArr.push(
                <TouchableOpacity key={i} onPress={()=>alert('点了')}>
                    <View style={styles.mineInnerViewStyle}>
                        <Text style={{color: 'white'}}>{item.number}</Text>
                        <Text style={{color: 'white'}}>{item.title}</Text>
                    </View>
                </TouchableOpacity>
            );
        }
        // 返回数组
        return itemArr;
    }
});


var styles = StyleSheet.create({
    viewStyle:{
        height:Platform.OS == 'ios'? 500: 150,
        backgroundColor:'rgba(255,96,0,1.0)',
    },

    topViewStyle:{
        marginTop:Platform.OS == 'ios'? 350 : 35,
        // 主轴的方向
        flexDirection:'row',
        // 侧轴居中
        alignItems:'center',
        // 设置主轴对齐方式
        justifyContent:'space-around'
    },

    topImgStyle:{
        width:Platform.OS == 'ios'? 80:60,
        height:Platform.OS == 'ios'? 80:60,
        borderRadius:Platform.OS == 'ios'? 40:30
    },

    middleViewStyle:{ // 中间的View
        //主轴方向
        flexDirection:'row',
        // 宽度
        width: width * 0.65,
        // 侧轴对齐方式
        alignItems:'center'
    },

    rankStyle:{
        width:17,
        height:17
    },

    /*-----------底部的View的样式-------------*/
    mineBottomViewStyle:{
        // 主轴的方向
        flexDirection:'row',
        // 背景颜色
        backgroundColor:'rgba(255,255,255,0.4)',
        // 高度
        height:44,
        // 侧轴居中
        alignItems:'center',
        // 绝对定位
        position:'absolute',
        bottom:0
    },

    mineInnerViewStyle:{
        width: width / 3.0 + 1,
        // 对齐方式
        justifyContent:'center',
        alignItems:'center',

        borderRightColor:'white',
        borderRightWidth:1
    }

});


module.exports = MineHeadView;