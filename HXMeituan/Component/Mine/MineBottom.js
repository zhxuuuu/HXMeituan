/**
 * Created by Zhxuuuu on 16/5/31.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Platform,
    Image,
    TouchableOpacity
} from 'react-native';

// 导入本地的json数据
var BottomViewData = require('./BottomView.json');

// 创建组件类
var MineBottomView = React.createClass({
    render(){
        return (
            <View style={styles.outViewStyle}>
                {this.renderAllItem()}
            </View>
        );
    },

    // 返回所有的子控件
    renderAllItem(){
        // 装所有组件的数组
        var itemArr = [];
        // 遍历数组
        for(var i=0; i<BottomViewData.length; i++){
            // 取出单个对象
            var item = BottomViewData[i];
            // 创建组件装入数组
            itemArr.push(
                <TouchableOpacity key={i} onPress={()=>alert('点了')}>
                    <View style={styles.innerViewStyle}>
                        <Image source={{uri:item.icon}} style={styles.imgStyle}/>
                        <Text style={{marginTop:5, color:'gray'}}>{item.title}</Text>
                    </View>
                </TouchableOpacity>
            );
        }
        // 返回数组
        return itemArr;
    }

});


// 创建样式类
var styles = StyleSheet.create({
    outViewStyle:{
        // 主轴的方向
        flexDirection:'row',
        // 设置主轴的对齐方式
        justifyContent:'space-around',
        // 背景颜色
        backgroundColor:'white',
        paddingTop:5,
        paddingBottom:5
    },

    innerViewStyle:{
        backgroundColor:'white',
        // 侧轴居中
        alignItems:'center'
    },

    imgStyle:{
        width: 50,
        height: 30,
        // 设置图片的内容模式
        resizeMode:'contain'
    }
});


// 输出模块
module.exports = MineBottomView