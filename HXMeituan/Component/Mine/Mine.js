/**
 * Created by Zhxuuuu on 16/5/31.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ScrollView
} from 'react-native';

// 导入外部的组件
var CommonCell = require('../More/CommonCell');
var MineBottomView = require('./MineBottom');
var MineHeadView = require('./MIneHeaderView');


// 创建组件类
var Mine = React.createClass({
    render(){
        return(
            <ScrollView
                style={{backgroundColor:'#e8e8e8'}}
                contentInset={{top:-300}}
                contentOffset={{y:300}}
            >
                {/*头部的View*/}
                <MineHeadView />
                {/*中间的内容*/}
                <View>
                    {/*上部分*/}
                    <CommonCell
                        mainTitle="我的订单"
                        subTitle="查看更多订单"
                        leftIcon="collect"
                    />
                    {/*下部分*/}
                    <MineBottomView />
                </View>

                {/*下边的内容*/}
                <View style={{marginTop:15}}>
                    <CommonCell
                        mainTitle = "我的钱包"
                        subTitle="账户余额:¥99.00"
                        leftIcon="draft"
                    />

                    <CommonCell
                        mainTitle = "抵用券"
                        subTitle="10"
                        leftIcon="like"
                    />
                </View>
                <View style={{marginTop:15}}>
                    <CommonCell
                        mainTitle = "积分商城"
                        leftIcon="card"
                    />
                </View>
                <View style={{marginTop:15}}>
                    <CommonCell
                        mainTitle = "今日推荐"
                        leftIcon="new_friend"
                        rightIcon="me_new"
                    />
                </View>
                <View style={{marginTop:15}}>
                    <CommonCell
                        mainTitle = "我要合作"
                        subTitle="轻松开店,招财进宝"
                        leftIcon="pay"
                    />
                </View>
            </ScrollView>
        );
    }
});


// 创建样式类
var styles = StyleSheet.create({
    outViewStyle:{
        flex:1,
        backgroundColor:'red'
    }
});


// 模块输出
module.exports = Mine;